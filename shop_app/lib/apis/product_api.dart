import 'dart:convert';
import 'package:http/http.dart';
import '../models/Product.dart';

class ProductApi {
  final String productURL = "http://192.168.1.66/products.php";

  Future<List> getPosts() async {
    Response res = await get(Uri.parse(productURL));

    if (res.statusCode == 200) {
      List<dynamic> body = jsonDecode(res.body);

      List products = body
        .map(
          (dynamic item) => Product.fromJson(item),
        )
        .toList();

      return products;
    } else {
      throw "Unable to retrieve posts.";
    }
  }
}