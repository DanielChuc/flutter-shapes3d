import 'package:flutter/material.dart';
import 'package:shop_app/screens/button_nav_bar.dart';
import 'package:shop_app/screens/form/quote_page.dart';
import 'package:shop_app/screens/home/home_screen.dart';
import 'package:shop_app/screens/payment/payment.dart';

import 'auth/login_page.dart';

Map<String, WidgetBuilder> obtenerRuta() {
  return <String, WidgetBuilder>{
    '/navigator': (BuildContext context) => ButtonNavBar(),
    '/loginPage': (BuildContext context) => LoginPage(),
    '/payment': (BuildContext context) => PaymentPage(),
    '/quote' : (BuildContext context) => QuotePage(),
    '/homeScreen': (BuildContext context) => HomeScreen(),
  };
}
