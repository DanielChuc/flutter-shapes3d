import 'package:flutter/material.dart';
import 'package:flutter_phone_direct_caller/flutter_phone_direct_caller.dart';
import 'package:email_launcher/email_launcher.dart';

class Contactus extends StatefulWidget {
  const Contactus({Key? key}) : super(key: key);

  @override
  State<Contactus> createState() => _ContactusState();
}

class _ContactusState extends State<Contactus> {
  final _toController = TextEditingController();
  final _ccController = TextEditingController();
  final _bccController = TextEditingController();
  final _subjectController = TextEditingController();
  final _bodyController = TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    _toController.dispose();
    _ccController.dispose();
    _bccController.dispose();
    _subjectController.dispose();
    _bodyController.dispose();
    super.dispose();
  }

  void _launchEmail() async {
    List<String> to = _toController.text.split(',');
    List<String> cc = _ccController.text.split(',');
    List<String> bcc = _bccController.text.split(',');
    String subject = _subjectController.text;
    String body = _bodyController.text;

    Email email = Email(to: to, cc: cc, bcc: bcc, subject: subject, body: body);
    EmailLauncher.launch(email).then((value) {
      // success
      print(value);
    }).catchError((error) {
      // has error
      print(error);
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          title: Text("Contactanos"),
          backgroundColor: Colors.cyan[800],
        ),
        body: SafeArea(
            child: ListView(
          padding: EdgeInsets.zero,
          shrinkWrap: true,
          scrollDirection: Axis.vertical,
          children: [
            Align(
              alignment: AlignmentDirectional(0, -0.6),
              child: Text(
                'Shapes 3D',
                style: TextStyle(fontSize: 30),
              ),
            ),
            Align(
              alignment: AlignmentDirectional(0, -0.95),
              child: Container(
                width: 180,
                height: 180,
                clipBehavior: Clip.antiAlias,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                ),
                child: Image.asset(
                  'assets/images/logo.jpeg',
                ),
              ),
            ),
            SizedBox(
              height: 80,
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Card(
                  clipBehavior: Clip.antiAliasWithSaveLayer,
                  color: Color(0xFFF5F5F5),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child: Center(
                      child: ElevatedButton.icon(
                    icon: Icon(Icons.phone),
                    label: Text("Contacto de Shapes 3D"),
                    style: ElevatedButton.styleFrom(
                      primary: Colors.teal,
                      onPrimary: Colors.white,
                      shape: const BeveledRectangleBorder(
                          borderRadius: BorderRadius.all(Radius.circular(5))),
                    ),
                    onPressed: () async {
                      FlutterPhoneDirectCaller.callNumber('+529992721211');
                    },
                  ))),
                  
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Expanded(
                child: ListView(
                  scrollDirection: Axis.vertical,
                  shrinkWrap: true,
                  children: [
                    _buildTextField(
                        _toController, TextInputType.emailAddress, 'Enter to'),
                    _buildTextField(
                        _ccController, TextInputType.emailAddress, 'Enter cc'),
                    _buildTextField(
                        _bccController, TextInputType.emailAddress, 'Enter bcc'),
                    _buildTextField(
                        _subjectController, TextInputType.text, 'Enter subject'),
                    _buildTextField(
                        _bodyController, TextInputType.text, 'Enter body'),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: ElevatedButton(
                          onPressed: _launchEmail, child: Text('Launch Email')),
                    )
                  ],
                ),
              ),
            )
          ],
        )),
      ),
    );
  }

  Widget _buildTextField(TextEditingController controller,
      TextInputType inputType, String hintText) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: TextField(
        controller: controller,
        keyboardType: inputType,
        decoration: InputDecoration(
            border: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.black54),
            ),
            hintText: hintText),
      ),
    );
  }
}
