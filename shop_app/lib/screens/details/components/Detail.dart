import 'package:flutter/material.dart';

import '../../../models/Product.dart';

class Detail extends StatelessWidget {
  const Detail({Key? key, required this.product}) : super(key: key);
  final Product product;


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(product.title),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Text(product.description),
      ),
    );;
  }
}