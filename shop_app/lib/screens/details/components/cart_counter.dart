import 'package:counter_button/counter_button.dart';
import 'package:flutter/material.dart';
class CartCounter extends StatefulWidget {
  CartCounter({
    Key? key,
  }) : super(key: key);

  @override
  State<CartCounter> createState() => _CartCounterState();
}

class _CartCounterState extends State<CartCounter> {
  int _counterValue = 0;
  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[buildOutlinedButton()],
    );
  }

  Center buildOutlinedButton() {
    return Center(
      child: CounterButton(
        loading: false,
        onChange: (int val) {
          setState(() {
            _counterValue = val;
          });
        },
        count: _counterValue,
        countColor: Colors.purple,
        buttonColor: Colors.purpleAccent,
        progressColor: Colors.purpleAccent,
      ),
    );
  }
}
