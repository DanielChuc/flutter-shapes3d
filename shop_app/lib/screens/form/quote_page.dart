// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';
import '../menu_drawer.dart';
import 'package:custom_radio_grouped_button/custom_radio_grouped_button.dart';
import 'package:http/http.dart' as http;
import 'dart:async';

class QuotePage extends StatefulWidget {
  QuotePage({Key? key}) : super(key: key);

  @override
  State<QuotePage> createState() => _QuotePageState();
}

class _QuotePageState extends State<QuotePage> {
  final nombre = TextEditingController();

  final apellido = TextEditingController();

  final empresa = TextEditingController();

  final direccion = TextEditingController();

  final correo = TextEditingController();

  final telefono = TextEditingController();

  final descripcion = TextEditingController();

  final altura = TextEditingController();

  final longitud = TextEditingController();

  final anchura = TextEditingController();

  String name = "";
  String lastname = "";
  String business = "";
  String address = "";
  String email = "";
  String phone = "";
  String description = "";
  String filament = "";
  dynamic height;
  dynamic length;
  dynamic width;
  dynamic shape;

  dynamic horas;
  dynamic tiempoMinutos;
  dynamic precio;
  dynamic errores;
  dynamic vmax = 4144;
  dynamic tmax = 2700;
  dynamic volume;
  dynamic filamentPrice;

//*dropdown list
  List<DropdownMenuItem<String>> get dropdownItems {
    List<DropdownMenuItem<String>> menuItems = [
      DropdownMenuItem(value: "PLA", child: Text("PLA - \$1.20 pesos MX")),
      DropdownMenuItem(value: "ABS", child: Text("ABS - \$1.50 pesos MX")),
      DropdownMenuItem(value: "PET", child: Text("PET - \$1.80 pesos MX")),
    ];
    return menuItems;
  }

  dynamic filamento = "PET";

  String? relleno;
  String? acabado;
  String figura = 'cubo';

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        home: Scaffold(
          drawer: MenuDrawer(),
          backgroundColor: Colors.white,
          appBar: buildAppBar(),
          body: Form(
            child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: ListView(
                  padding: EdgeInsets.symmetric(vertical: 20),
                  children: <Widget>[
                    Text(
                      'Datos Personales',
                      style: TextStyle(
                        fontWeight: FontWeight.w400,
                        fontSize: 25,
                      ),
                      textAlign: TextAlign.center,
                    ),
                    const SizedBox(
                      height: 15,
                    ),
                    TextFormField(
                      controller: nombre,
                      decoration: const InputDecoration(
                          labelText: "Nombre",
                          border:
                              OutlineInputBorder() /* (borderRadius: BorderRadius.all(Radius.circular(20))) */
                          ),
                    ),
                    const SizedBox(
                      height: 15,
                    ),
                    TextFormField(
                      controller: apellido,
                      decoration: const InputDecoration(
                          labelText: "Apellido",
                          border:
                              OutlineInputBorder() /* (borderRadius: BorderRadius.all(Radius.circular(20))) */
                          ),
                    ),
                    const SizedBox(
                      height: 15,
                    ),
                    TextFormField(
                      controller: empresa,
                      decoration: const InputDecoration(
                          labelText: "Empresa",
                          border:
                              OutlineInputBorder() /* (borderRadius: BorderRadius.all(Radius.circular(20))) */
                          ),
                    ),
                    const SizedBox(
                      height: 15,
                    ),
                    TextFormField(
                      controller: direccion,
                      decoration: const InputDecoration(
                          labelText: "Dirección",
                          border:
                              OutlineInputBorder() /* (borderRadius: BorderRadius.all(Radius.circular(20))) */
                          ),
                    ),
                    const SizedBox(
                      height: 15,
                    ),
                    TextFormField(
                      controller: correo,
                      decoration: const InputDecoration(
                          labelText: "Correo Electrónico",
                          border:
                              OutlineInputBorder() /* (borderRadius: BorderRadius.all(Radius.circular(20))) */
                          ),
                    ),
                    const SizedBox(
                      height: 15,
                    ),
                    TextFormField(
                      controller: telefono,
                      decoration: const InputDecoration(
                          labelText: "Teléfono",
                          border:
                              OutlineInputBorder() /* (borderRadius: BorderRadius.all(Radius.circular(20))) */
                          ),
                      keyboardType: TextInputType.number,
                      inputFormatters: <TextInputFormatter>[
                        FilteringTextInputFormatter.digitsOnly
                      ],
                    ),
                    const SizedBox(
                      height: 15,
                    ),
                    TextFormField(
                      maxLines: 4,
                      controller: descripcion,
                      decoration: const InputDecoration(
                          labelText: "Dirección", border: OutlineInputBorder()),
                    ),
                    const SizedBox(
                      height: 15,
                    ),
                    TextFormField(
                      controller: altura,
                      decoration: const InputDecoration(
                          labelText: "Altura",
                          border:
                              OutlineInputBorder() /* (borderRadius: BorderRadius.all(Radius.circular(20))) */
                          ),
                      textInputAction: TextInputAction.send,
                      keyboardType: TextInputType.number,
                      inputFormatters: <TextInputFormatter>[
                        FilteringTextInputFormatter.digitsOnly
                      ],
                    ),
                    const SizedBox(
                      height: 15,
                    ),
                    TextFormField(
                      controller: longitud,
                      decoration: const InputDecoration(
                          labelText: "Longitud",
                          border:
                              OutlineInputBorder() /* (borderRadius: BorderRadius.all(Radius.circular(20))) */
                          ),
                      keyboardType: TextInputType.number,
                      inputFormatters: <TextInputFormatter>[
                        FilteringTextInputFormatter.digitsOnly
                      ],
                    ),
                    const SizedBox(
                      height: 15,
                    ),
                    TextFormField(
                      controller: anchura,
                      decoration: const InputDecoration(
                          labelText: "Anchura",
                          border:
                              OutlineInputBorder() /* (borderRadius: BorderRadius.all(Radius.circular(20))) */
                          ),
                      keyboardType: TextInputType.number,
                      inputFormatters: <TextInputFormatter>[
                        FilteringTextInputFormatter.digitsOnly
                      ], // Only numbers can be entered
                    ),
                    const SizedBox(
                      height: 25,
                    ),
                    ElevatedButton(
                      onPressed: null,
                      child: Text("Seleccione el archivo"),
                    ),
                    const SizedBox(
                      height: 15,
                    ),
                    TextFormField(
                      maxLines: 5,
                      controller: descripcion,
                      decoration: const InputDecoration(
                          labelText: "Descripción",
                          border:
                              OutlineInputBorder() /* (borderRadius: BorderRadius.all(Radius.circular(20))) */
                          ),
                    ),
                    const SizedBox(
                      height: 25,
                    ),
                    DropdownButtonFormField<String>(
                        decoration: InputDecoration(
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(20),
                          ),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(20),
                          ),
                          filled: true,
                        ),
                        value: filamento,
                        onChanged: (String? newValue) {
                          setState(() {
                            filamento = newValue!;
                            print(filamento);
                          });
                        },
                        items: dropdownItems),
                    const SizedBox(
                      height: 25,
                    ),
                    Text(
                      'Relleno',
                      style: TextStyle(
                        fontWeight: FontWeight.w400,
                        fontSize: 25,
                      ),
                      textAlign: TextAlign.center,
                    ),
                    const SizedBox(
                      height: 25,
                    ),
                    CustomRadioButton(
                      elevation: 0,
                      absoluteZeroSpacing: true,
                      unSelectedColor: Theme.of(context).canvasColor,
                      buttonLables: const [
                        '20-30',
                        '50-60',
                        '90-100',
                      ],
                      buttonValues: const [
                        "20-30",
                        "50-60",
                        "90-100",
                      ],
                      buttonTextStyle: ButtonTextStyle(
                          selectedColor: Colors.white,
                          unSelectedColor: Colors.black,
                          textStyle: TextStyle(fontSize: 16)),
                      radioButtonValue: (value) {
                        setState(() {
                          relleno = value as String?;
                          print(relleno);
                        });
                      },
                      selectedColor: Theme.of(context).colorScheme.secondary,
                      enableShape: true,
                    ),
                    const SizedBox(
                      height: 25,
                    ),
                    Text(
                      'Acabado Superficial',
                      style: TextStyle(
                        fontWeight: FontWeight.w400,
                        fontSize: 25,
                      ),
                      textAlign: TextAlign.center,
                    ),
                    const SizedBox(
                      height: 25,
                    ),
                    CustomRadioButton(
                      elevation: 0,
                      absoluteZeroSpacing: true,
                      unSelectedColor: Theme.of(context).canvasColor,
                      buttonLables: const [
                        'Fino',
                        'Medio',
                        'Rugoso',
                      ],
                      buttonValues: const [
                        "fino",
                        "medio",
                        "rugoso",
                      ],
                      buttonTextStyle: ButtonTextStyle(
                          selectedColor: Colors.white,
                          unSelectedColor: Colors.black,
                          textStyle: TextStyle(fontSize: 16)),
                      radioButtonValue: (value) {
                        setState(() {
                          acabado = value as String?;
                          print(acabado);
                        });
                      },
                      selectedColor: Theme.of(context).colorScheme.secondary,
                      enableShape: true,
                    ),
                    const SizedBox(
                      height: 25,
                    ),
                    Text(
                      'Acabado Superficial',
                      style: TextStyle(
                        fontWeight: FontWeight.w400,
                        fontSize: 25,
                      ),
                      textAlign: TextAlign.center,
                    ),
                    const SizedBox(
                      height: 25,
                    ),
                    Row(
                      children: [
                        GestureDetector(
                          onTap: () => setState(() => figura = 'piramide'),
                          child: Container(
                            height: 175,
                            width: 160,
                            color: figura == 'piramide'
                                ? Colors.grey
                                : Colors.transparent,
                            child: Image.asset("assets/images/pyramid.png"),
                          ),
                        ),
                        SizedBox(width: 4),
                        GestureDetector(
                          onTap: () => setState(() => figura = 'cubo'),
                          child: Container(
                            height: 175,
                            width: 160,
                            color: figura == 'cubo'
                                ? Colors.grey
                                : Colors.transparent,
                            child: Image.asset(
                              "assets/images/block.png",
                            ),
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(height: 25),
                    Center(
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 10),
                        child: Column(
                          children: [
                            SizedBox(
                              height: 50,
                            ),
                            ElevatedButton(
                              onPressed: () => _calculate(context),
                              style: ElevatedButton.styleFrom(
                                primary: Color.fromARGB(255, 20, 141, 24),
                              ),
                              child: Text("Solocitar Cotización",
                                  style: TextStyle(fontSize: 17)),
                            ),
                            ElevatedButton(
                              onPressed: () {
                                // Respond to button press
                              },
                              style: ElevatedButton.styleFrom(
                                primary: Color.fromARGB(255, 182, 36, 26),
                              ),
                              child: Text("¿Quieres continuar con la compra?",
                                  style: TextStyle(fontSize: 17)),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                )),
          ),
        ));
  }

  AppBar buildAppBar() {
    return AppBar(
      title: const Text("Cotización"),
      backgroundColor: Colors.cyan[800],
      elevation: 0,
      actions: <Widget>[
        IconButton(
          color: Colors.grey,
          icon: SvgPicture.asset('assets/icons/search.svg'),
          onPressed: () {},
        ),
        IconButton(
          icon: SvgPicture.asset('assets/icons/cart.svg'),
          onPressed: () {},
        ),
        const SizedBox(
          width: 10,
        )
      ],
    );
  }

  void _calculate(BuildContext context) {
    name = nombre.text;
    lastname = apellido.text;
    business = empresa.text;
    address = direccion.text;
    email = correo.text;
    phone = telefono.text;
    description = descripcion.text;

    height = int.parse(altura.text);
    length = int.parse(longitud.text);
    width = int.parse(anchura.text);
    filament = filamento;
    shape = figura;

    switch (filament) {
      case "ABS":
        {
          filamentPrice = 1.50;
        }
        break;
      case "PLA":
        {
          filamentPrice = 1.20;
        }
        break;
      case "PET":
        {
          filamentPrice = 1.80;
        }
        break;
      default:
        break;
    }

/*     volume = length * height * width;
    tiempoMinutos = (volume * tmax) / vmax;
    horas = tiempoMinutos / 60;
    precio = tiempoMinutos * filamentPrice; */

    if (shape == 'cubo') {
      volume = length * height * width;
      tiempoMinutos = (volume * tmax) / vmax;
      horas = tiempoMinutos / 60;
      precio = tiempoMinutos * filamentPrice;
    } else {
      volume = ((length * height) / 2) * width;
      tiempoMinutos = (volume * tmax) / vmax;
      horas = tiempoMinutos / 60;
      precio = tiempoMinutos * filamentPrice;
    }

volume = double.parse((volume).toStringAsFixed(2));
tiempoMinutos = tiempoMinutos.toInt();
horas = horas.toInt();
precio = precio.toInt();


    showDialog(
        context: context,
        builder: (BuildContext) {
          return AlertDialog(
            title: Text("El volumen completo es de ${volume.toString()} cm2"),
            content: SingleChildScrollView(
                child: ListBody(
              children: [
                Text("El precio es de \$${precio.toString()}"),
                Text("Total de minutos para imprimir es de ${tiempoMinutos.toString()} Minutos"),
                Text("Total de Horas para imprimir es de ${horas.toString()} Horas"),

              ],
            )),
            actions: <Widget>[
              TextButton(
                onPressed: () => Navigator.pop(context, 'Aceptar'),
                child: const Text('Aceptar'),
              ),
            ],
          );
        });
  }
}
