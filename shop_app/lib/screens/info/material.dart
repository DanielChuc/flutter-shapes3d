import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class MaterialsPage extends StatefulWidget {
  MaterialsPage({Key? key}) : super(key: key);

  @override
  State<MaterialsPage> createState() => _MaterialsPageState();
}

class _MaterialsPageState extends State<MaterialsPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(
        title: const Text('Materiales'),
        backgroundColor: Colors.cyan[800],
      elevation: 0,
      
      actions: <Widget>[
        IconButton(
          color: Colors.grey,
          icon: SvgPicture.asset('assets/icons/search.svg'),
          onPressed: () {},
        ),
        IconButton(
          icon: SvgPicture.asset('assets/icons/cart.svg'),
          onPressed: () {},
        ),
        SizedBox(width: 10,)
      ],
      ),
      body: Center(child: Image.asset('assets/images/filament-comparison.png')),
    );
  }
}
