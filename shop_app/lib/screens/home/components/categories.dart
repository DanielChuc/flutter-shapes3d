import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:http/http.dart' as http;
import 'package:shop_app/screens/home/components/product_category_card.dart';
import '../../../models/CategoryProduct.dart';
import '../../../size_config.dart';

class Categories extends StatefulWidget {
  const Categories({Key? key}) : super(key: key);

  @override
  State<Categories> createState() => _CategoriesState();
}

class _CategoriesState extends State<Categories> {
  List<CategoryProduct> categories = <CategoryProduct>[];

  Future<List<CategoryProduct>> getCategories() async {
    var url = "http://192.168.1.66/categories.php";

    var response =
        await http.post(Uri.parse(url)).timeout(Duration(seconds: 90));

    var datos = json.decode(response.body);

    var registros = <CategoryProduct>[];

    for (datos in datos) {
      registros.add(CategoryProduct.fromJson(datos));
    }
    return registros;
  }

  @override
  void initState() {
    super.initState();
    getCategories().then((value) {
      setState(() {
        categories.addAll(value);
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    /*   List<Map<String, dynamic>> categories = [
      {"icon": "assets/icons/BillIcon.svg", "text": "Hogar"},
      {"icon": "assets/icons/BillIcon.svg", "text": "Escolar"},
      {"icon": "assets/icons/BillIcon.svg", "text": "Juego"},
      {"icon": "assets/icons/BillIcon.svg", "text": "Tecnología"},
      {"icon": "assets/icons/BillIcon.svg", "text": "Popular"},
    ]; */

    return Padding(
      padding: EdgeInsets.all(20),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: List.generate(
          categories.length,
          (index) => CategoryCard(
            icon: categories[index].icon,
            text: categories[index].name,
            press: () {
             /*  Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => ProductCategoryCard(
                    categoryProducts: categories[index],
                  ),
                ),
              ); */
            },
          ),
        ),
      ),
    );
  }
}

class CategoryCard extends StatelessWidget {
  const CategoryCard({
    Key? key,
    required this.icon,
    required this.text,
    required this.press,
  }) : super(key: key);

  final String? icon, text;
  final GestureTapCallback press;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: press,
      child: SizedBox(
        width: 55,
        child: Column(
          children: [
            Container(
              padding: EdgeInsets.all(15),
              height: 55,
              width: 55,
              decoration: BoxDecoration(
                color: Color(0xFFFFECDF),
                borderRadius: BorderRadius.circular(10),
              ),
              child: SvgPicture.asset(icon!),
            ),
            SizedBox(height: 5),
            Text(text!, textAlign: TextAlign.center)
          ],
        ),
      ),
    );
  }
}
