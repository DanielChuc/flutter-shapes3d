/* import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import '../../../models/CategoryProduct.dart';
import '../../../models/Product.dart';

class ProductCategoryCard extends StatefulWidget {
  final CategoryProduct categoryProducts;

  ProductCategoryCard({
    Key? key,
    required this.categoryProducts,
  }) : super(key: key);
  @override
  State<ProductCategoryCard> createState() => _ProductCategoryCardState();
}

class _ProductCategoryCardState extends State<ProductCategoryCard> {
  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: ListView.builder(
        itemCount: categoryProducts.length,
        itemBuilder: (context, index) {
          return Container(
            child: Card(
              child: Container(
                height: 100,
                color: Colors.white,
                child: Row(
                  children: [
                    Center(
                      child: Padding(
                        padding: EdgeInsets.all(10),
                        child: Expanded(
                          child: FadeInImage(
                              placeholder:
                                  AssetImage('assets/images/Loading.gif'),
                              image: AssetImage(data[index].image),
                              height: 80,
                              fit: BoxFit.cover),
                        ),
                      ),
                    ),
                    Expanded(
                      child: Container(
                        alignment: Alignment.topLeft,
                        child: Column(
                          children: [
                            Expanded(
                              flex: 5,
                              child: ListTile(
                                title: Text(data[index].title),
                                subtitle: Text("\$" + data[index].price),
                              ),
                            ),
                            Expanded(
                              flex: 5,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: [
                                  TextButton(
                                    child: Text("Comprar"),
                                    onPressed: () {},
                                  ),
                                  SizedBox(
                                    width: 8,
                                  ),
                                  TextButton(
                                    child: Text("Añadir a la lista"),
                                    onPressed: () {},
                                  ),
                                  SizedBox(
                                    width: 8,
                                  )
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                      flex: 8,
                    ),
                  ],
                ),
              ),
              elevation: 8,
              margin: EdgeInsets.all(10),
            ),
          );
        },
      ),
    );
  }
}
 */