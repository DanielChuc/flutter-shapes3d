import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:shop_app/screens/home/components/discount_banner.dart';
import 'package:favorite_button/favorite_button.dart';
import 'package:shop_app/screens/home/components/special_offers.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../../../models/Product.dart';

import '../../details/details_screen.dart';
import 'package:http/http.dart' as http;
import 'categories.dart';

class Body extends StatefulWidget {
  const Body({Key? key}) : super(key: key);

  @override
  State<Body> createState() => _BodyState();
}

class _BodyState extends State<Body> {
  List<Product> data = <Product>[];

  Future<List<Product>> tomar_datos() async {
    var url = "http://192.168.1.66/products.php";

    var response =
        await http.post(Uri.parse(url)).timeout(Duration(seconds: 90));

    var datos = json.decode(response.body);

    var registros = <Product>[];

    for (datos in datos) {
      registros.add(Product.fromJson(datos));
    }
    return registros;
  }

 

  @override
  void initState() {
    super.initState();
    tomar_datos().then((value) {
      setState(() {
        data.addAll(value);
      });
    });
  
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20),
        ),
        DiscountBanner(),
        SpecialOffers(),
        Categories(),
        Expanded(
            child: SafeArea(
          child: ListView.builder(
              scrollDirection: Axis.horizontal,
              padding: EdgeInsets.symmetric(horizontal: 16),
              itemCount: data.length,
              itemBuilder: (context, index) {
                return GestureDetector(
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => DetailsScreen(
                          product: data[index],
                        ),
                      ),
                    );
                  },
                  child: Container(
                    width: 200,
                    height: 50,
                    //color: Colors.red,
                    child: Expanded(
                      child: Column(
                        children: [
                          AspectRatio(
                            aspectRatio: 4 / 3,
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(20),
                              child: FadeInImage(
                                placeholder:
                                    AssetImage('assets/images/Loading.gif'),
                                image: AssetImage(data[index].image),
                                height: 150,
                              ),
                            ),
                          ),
                          const SizedBox(
                            height: 4,
                          ),
                          Text(
                            data[index].title,
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              RichText(
                                text: TextSpan(
                                  text: "\$${data[index].price}",
                                  style: TextStyle(
                                    color: Colors.grey[800],
                                  ),
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.symmetric(horizontal: 20),
                                child: FavoriteButton(
                                  iconSize: 30,
                                  isFavorite: false,
                                  // iconDisabledColor: Colors.white,
                                  valueChanged: (_isFavorite) {
                                    print('Is Favorite : $_isFavorite');
                                  },
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                );
              }),
        ))
      ],
    );
  }
}
