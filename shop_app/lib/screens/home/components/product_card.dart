import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import '../../../models/Product.dart';

class ProductCard extends StatefulWidget {
  ProductCard({Key? key}) : super(key: key);

  @override
  State<ProductCard> createState() => _ProductCardState();
}

class _ProductCardState extends State<ProductCard> {
    List<Product> data = <Product>[];

  Future<List<Product>> tomar_datos() async {
    var url = "http://192.168.1.164/products.php";

    var response =
        await http.post(Uri.parse(url)).timeout(Duration(seconds: 90));

    var datos = json.decode(response.body);

    var registros = <Product>[];

    for (datos in datos) {
      registros.add(Product.fromJson(datos));
    }
    return registros;
  }

  @override
  void initState() {
    super.initState();
    tomar_datos().then((value) {
      setState(() {
        data.addAll(value);
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: ListView.builder(
            itemCount: data.length,
            itemBuilder: (context, index) {
              return Container(
                child: Card(
                  child: Container(
                    height: 100,
                    color: Colors.white,
                    child: Row(
                      children: [
                        Center(
                          child: Padding(
                            padding: EdgeInsets.all(10),
                            child: Expanded(
                              child: FadeInImage(
                                  placeholder:
                                      AssetImage('assets/images/Loading.gif'),
                                  image: AssetImage(data[index].image),
                                  height: 80,
                                  fit: BoxFit.cover),
                            ),
                          ),
                        ),
                        Expanded(
                          child: Container(
                            alignment: Alignment.topLeft,
                            child: Column(
                              children: [
                                Expanded(
                                  flex: 5,
                                  child: ListTile(
                                    title: Text(data[index].title),
                                    subtitle: Text("\$" + data[index].price),
                                  ),
                                ),
                                Expanded(
                                  flex: 5,
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: [
                                      TextButton(
                                        child: Text("Comprar"),
                                        onPressed: () {},
                                      ),
                                      SizedBox(
                                        width: 8,
                                      ),
                                      TextButton(
                                        child: Text("Añadir a la lista"),
                                        onPressed: () {},
                                      ),
                                      SizedBox(
                                        width: 8,
                                      )
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ),
                          flex: 8,
                        ),
                      ],
                    ),
                  ),
                  elevation: 8,
                  margin: EdgeInsets.all(10),
                ),
              );
            },
          ),
    );
  }
}