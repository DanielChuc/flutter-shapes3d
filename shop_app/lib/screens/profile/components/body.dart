import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import '../../../models/Profile.dart';
import 'profile_menu.dart';
import 'profile_pic.dart';

class Body extends StatefulWidget {
  @override
  State<Body> createState() => _BodyState();
}

class _BodyState extends State<Body> {
  List<Profile> profile = <Profile>[];

  Future<List<Profile>> getProfile() async {
    late SharedPreferences prefs;

    String name = "";
    prefs = await SharedPreferences.getInstance();
    name = prefs.getString("username")!;

    var url = 'http://192.168.1.66/profile.php';
    var response = await http.post(Uri.parse(url), body: {
      'username': name,
    });

    var datos = json.decode(response.body);

    var registros = <Profile>[];

    for (datos in datos) {
      registros.add(Profile.fromJson(datos));
    }
    return registros;
  }

  @override
  void initState() {
    super.initState();
    getProfile().then((value) {
      setState(() {
        profile.addAll(value);
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      padding: const EdgeInsets.symmetric(vertical: 20),
      child: Column(
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              ListView.builder(
                  scrollDirection: Axis.vertical,
                  shrinkWrap: true,
                  itemCount: profile.length,
                  itemBuilder: (context, index) {
                    return Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20),
                      child: Column(
                        children: [
                          SizedBox(
                            height: 115,
                            width: 115,
                            child: Stack(
                              fit: StackFit.expand,
                              clipBehavior: Clip.none,
                              children: [
                                CircleAvatar(
                                  backgroundImage:
                                      NetworkImage(profile[index].image),
                                ),
                                Positioned(
                                  right: -16,
                                  bottom: 0,
                                  child: SizedBox(
                                    height: 46,
                                    width: 46,
                                    child: TextButton(
                                      style: TextButton.styleFrom(
                                        shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(50),
                                          side: BorderSide(color: Colors.white),
                                        ),
                                        primary: Colors.white,
                                        backgroundColor: Color(0xFFF5F6F9),
                                      ),
                                      onPressed: () {},
                                      child: SvgPicture.asset(
                                          "assets/icons/CameraIcon.svg"),
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                          const SizedBox(
                            height: 15,
                          ),
                          ListTile(
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(20.0)),
                            selected: true,
                            selectedTileColor: Colors.grey[100],
                            title: Text(
                              profile[index].name  == ""
                                  ? "Ingresa tu nombre"
                                  : profile[index].name,
                              style: const TextStyle(color: Colors.black),
                            ),
                            leading: const Icon(Icons.person),
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          ListTile(
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(20.0)),
                            selected: true,
                            selectedTileColor: Colors.grey[100],
                            title: Text(
                              profile[index].lastname == ""
                                  ? "Ingresa tu apellido"
                                  : profile[index].lastname,
                              style: const TextStyle(color: Colors.black),
                            ),
                            leading: const Icon(Icons.person),
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          ListTile(
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(20.0)),
                            selected: true,
                            selectedTileColor: Colors.grey[100],
                            title: Text(
                              profile[index].address == ""
                                  ? "Ingresa tu dirección"
                                  : profile[index].address,
                              style: const TextStyle(color: Colors.black),
                            ),
                            leading: const Icon(Icons.pin_drop),
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          ListTile(
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(20.0)),
                            selected: true,
                            selectedTileColor: Colors.grey[100],
                            title: Text(
                              profile[index].postal == ""
                                  ? "Ingresa tu código postal"
                                  : profile[index].postal,
                              style: const TextStyle(color: Colors.black),
                            ),
                            leading: const Icon(Icons.markunread_mailbox),
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          ListTile(
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(20.0)),
                            selected: true,
                            selectedTileColor: Colors.grey[100],
                            title: Text(
                              profile[index].country == ""
                                  ? "Ingresa tu país"
                                  : profile[index].country,
                              style: const TextStyle(color: Colors.black),
                            ),
                            leading: const Icon(Icons.flag),
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          ListTile(
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(20.0)),
                            selected: true,
                            selectedTileColor: Colors.grey[100],
                            title: Text(
                              profile[index].state == ""
                                  ? "Ingresa tu estado"
                                  : profile[index].state,
                              style: const TextStyle(color: Colors.black),
                            ),
                            leading: const Icon(Icons.map_sharp),
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          ListTile(
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(20.0)),
                            selected: true,
                            selectedTileColor: Colors.grey[100],
                            title: Text(
                              profile[index].city == ""
                                  ? "Ingresa tu ciudad"
                                  : profile[index].city,
                              style: const TextStyle(color: Colors.black),
                            ),
                            leading: const Icon(Icons.location_city),
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          ListTile(
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(20.0)),
                            selected: true,
                            selectedTileColor: Colors.grey[100],
                            title: Text(
                              profile[index].phone == ""
                                  ? "Ingresa tu número de celular"
                                  : profile[index].phone,
                              style: const TextStyle(color: Colors.black),
                            ),
                            leading: const Icon(Icons.call),
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          ListTile(
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(20.0)),
                            selected: true,
                            selectedTileColor: Colors.grey[100],
                            title: Text(
                              profile[index].house_references == ""
                                  ? "Ingresa tu referencias"
                                  : profile[index].house_references,
                              style: const TextStyle(color: Colors.black),
                            ),
                            leading: const Icon(Icons.menu_book),
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                        ],
                      ),
                    );
                  })
            ],
          )
        ],
      ),
    );
  }
}
