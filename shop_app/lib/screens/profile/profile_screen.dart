import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:shop_app/screens/profile/components/body.dart';
import 'package:http/http.dart' as http;
import '../../models/Profile.dart';
import '../button_nav_bar.dart';
import '../menu_drawer.dart';

class ProfileScreen extends StatefulWidget {
  const ProfileScreen({Key? key}) : super(key: key);

  @override
  State<ProfileScreen> createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
 

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        home: Scaffold(
          drawer: MenuDrawer(),
          backgroundColor: Colors.white,
          appBar: buildAppBar(),
          body: Body(),
        ));
  }

  AppBar buildAppBar() {
    return AppBar(
      title: Text("Profile"),
      backgroundColor: Colors.cyan[800],
      elevation: 0,
      actions: <Widget>[
        IconButton(
          color: Colors.grey,
          icon: SvgPicture.asset('assets/icons/search.svg'),
          onPressed: () {},
        ),
        IconButton(
          icon: SvgPicture.asset('assets/icons/cart.svg'),
          onPressed: () {},
        ),
        SizedBox(
          width: 10,
        )
      ],
    );
  }
}
