import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:flutter/material.dart';
import 'package:shop_app/examples/noti.dart';
import 'package:shop_app/examples/settings.dart';
import 'package:shop_app/screens/home/home_screen.dart';

class ButtonNavBar extends StatefulWidget {
  ButtonNavBar({Key? key}) : super(key: key);

  @override
  State<ButtonNavBar> createState() => _ButtonNavBarState();
}

int _page = 0;
List<Widget> _paginas = [
  //*Aqui vas agregando las paginas que quieres

  HomeScreen(),
  Noti(),
  Settings()
];

class _ButtonNavBarState extends State<ButtonNavBar> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _paginas[_page],
      bottomNavigationBar: CurvedNavigationBar(
        backgroundColor: Colors.white,
        color: Color.fromARGB(255, 12, 128, 143),
        animationDuration: Duration(milliseconds: 300),
        onTap: (index) {
          setState(() {
            _page = index;
          });
        },
        letIndexChange: (index) => true,
        items: const [
          Icon(
            Icons.home,
            color: Colors.white,
          ),
          Icon(Icons.notifications_active, color: Colors.white),
          Icon(Icons.settings, color: Colors.white),
        ],
      ),
    );
  }
}
