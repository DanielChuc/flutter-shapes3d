import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:shop_app/auth/login_page.dart';
import 'package:shop_app/examples/noti.dart';
import 'package:http/http.dart' as http;
import 'package:shop_app/screens/button_nav_bar.dart';
import 'package:shop_app/screens/home/home_screen.dart';
import 'package:shop_app/screens/profile/components/profile_menu.dart';
import 'package:shop_app/screens/profile/profile_screen.dart';
import '../models/User.dart';
import 'contactus.dart';

class MenuDrawer extends StatefulWidget {
  const MenuDrawer({Key? key}) : super(key: key);

  @override
  State<MenuDrawer> createState() => _MenuDrawerState();
}

class _MenuDrawerState extends State<MenuDrawer> {
  List<User> user = <User>[];

  Future<List<User>> getUser() async {
    late SharedPreferences prefs;

    String name = "";
    prefs = await SharedPreferences.getInstance();
    name = prefs.getString("username")!;

    var url = 'http://192.168.1.66/user.php';
    var response = await http.post(Uri.parse(url), body: {
      'username': name,
    });

    var datos = json.decode(response.body);

    var registros = <User>[];

    for (datos in datos) {
      registros.add(User.fromJson(datos));
    }
    return registros;
  }

  @override
  void initState() {
    super.initState();
    getUser().then((value) {
      setState(() {
        user.addAll(value);
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 250,
      child: Drawer(
        backgroundColor: Colors.white,
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            ListView.builder(
              scrollDirection: Axis.vertical,
              shrinkWrap: true,
              itemCount: user.length,
              itemBuilder: (context, index) {
                return Center(
                  child: UserAccountsDrawerHeader(
                    decoration: BoxDecoration(color: Colors.cyan[800]),
                    accountName: Text(user[index].username),
                    accountEmail: Text(user[index].email),
                    currentAccountPicture: CircleAvatar(
                      radius: 50.0,
                      backgroundColor: Color.fromARGB(255, 255, 255, 255),
                      backgroundImage: NetworkImage(user[index].image),
                    ),
                  ),
                );
              },
            ),
            ProfileMenu(
              text: "Home",
              icon: "assets/icons/home.svg",
              press: () => {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => ButtonNavBar()),
                )
              },
            ),
            ProfileMenu(
              text: "Mi Perfil",
              icon: "assets/icons/UserIcon.svg",
              press: () => {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => const ProfileScreen()),
                )
              },
            ),
            ProfileMenu(
              text: "Mis compras",
              icon: "assets/icons/bag.svg",
              press: () {},
            ),
            ProfileMenu(
              text: "Lista de deseos",
              icon: "assets/icons/HeartIcon.svg",
              press: () {},
            ),
            ProfileMenu(
              text: "Mis modelos 3D",
              icon: "assets/icons/printer3d.svg",
              press: () {},
            ),
            ProfileMenu(
              text: "Notificaciones",
              icon: "assets/icons/Bell.svg",
              press: () {},
            ),
            ProfileMenu(
              text: "Ajustes",
              icon: "assets/icons/Settings.svg",
              press: () {},
            ),
            ProfileMenu(
              text: "Ayuda",
              icon: "assets/icons/Questionmark.svg",
              press: () {},
            ),
            ProfileMenu(
              text: "Cerrar Sesión",
              icon: "assets/icons/Logout.svg",
              press: () async {
                SharedPreferences prefs = await SharedPreferences.getInstance();
                prefs.remove('username');
                // ignore: use_build_context_synchronously
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => LoginPage()),
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}
