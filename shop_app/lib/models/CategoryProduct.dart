class CategoryProduct {
  final String idCategory;
  final String name;
  final String descriptionCategory;
  final String icon;
  final String idProduct;
  final String title;
  final String image;
  final String description;
  final String price;
  final String size;
  final String color;

  CategoryProduct({
    required this.idCategory,
    required this.name,
    required this.descriptionCategory,
    required this.icon,
    required this.idProduct,
    required this.title,
    required this.image,
    required this.price,
    required this.description,
    required this.size,
    required this.color,
  });

  factory CategoryProduct.fromJson(Map<String, dynamic> json) {
    return CategoryProduct(
        idCategory: json["idCategory"],
        name: json["name"],
        descriptionCategory: json["descriptionCategory"],
        icon: json["icon"],
        idProduct: json['idProduct'],
        title: json['title'],
        image: json['image'],
        description: json['description'],
        price: json['price'],
        size: json['size'],
        color: json['color']);
  }


}
