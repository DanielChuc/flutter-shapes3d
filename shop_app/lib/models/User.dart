class User {
  final String username;
  final String email;
  final String image;

  

  User(
      {required this.username,
      required this.email,
      required this.image,

      });

  factory User.fromJson(Map<String, dynamic> json) {
    return User(
      username: json["username"],
      email: json["email"],
      image: json["image"],
    );
  }
}
