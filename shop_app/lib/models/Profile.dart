class Profile {
  final String name;
  final String lastname;
  final String address;
  final String postal;
  final String country;
  final String city;
  final String phone;
  final String state;
  final String image;
  final String house_references;

  Profile({
    required this.name,
    required this.lastname,
    required this.address,
    required this.postal,
    required this.country,
    required this.city,
    required this.phone,
    required this.state,
    required this.image,
    required this.house_references,
  });

  factory Profile.fromJson(Map<String, dynamic> json) {
    return Profile(
      name: json["name"],
      lastname: json["lastname"],
      address: json["address"],
      postal: json["postal"],
      country: json["country"],
      city: json["city"],
      phone: json["phone"],
      state: json["state"],
      image: json["image"],
      house_references: json["house_references"],
    );
  }
}
