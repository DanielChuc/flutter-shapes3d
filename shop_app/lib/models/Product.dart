import 'package:flutter/material.dart';

class Product {
  final String image, title, description, id,price, size,color;
  

  Product({
    required this.id,
    required this.title,
    required this.image,
    required this.price,
    required this.description,
    required this.size,
    required this.color,
  });
  factory Product.fromJson(Map<String, dynamic> json) {
    return Product(
        id: json['id'],
        title: json['title'],
        image: json['image'],
        description: json['description'],
        price: json['price'],
        size: json['size'],
        color: json['color']);
  }
}
