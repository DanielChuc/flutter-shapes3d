class Category {
  final String id;
  final String name;
  final String descriptionCategory;
  final String icon;

  Category({
    required this.id,
    required this.name,
    required this.descriptionCategory,
    required this.icon,
  });

  factory Category.fromJson(Map<String, dynamic> json) {
    return Category(
      id: json["id"],
      name: json["name"],
      descriptionCategory: json["descriptionCategory"],
      icon: json["icon"],
    );
  }
}
