import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import '../screens/menu_drawer.dart';

class Noti extends StatelessWidget {
  const Noti({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        drawer: MenuDrawer(),

      backgroundColor: Colors.white,
      appBar: buildAppBar(),
      body: Text("data"),
    ));
  }

  AppBar buildAppBar() {
    return AppBar(
      title: Text("Notifications"),
      backgroundColor: Colors.cyan[800],
      elevation: 0,
      
      actions: <Widget>[
        IconButton(
          color: Colors.grey,
          icon: SvgPicture.asset('assets/icons/search.svg'),
          onPressed: () {},
        ),
        IconButton(
          icon: SvgPicture.asset('assets/icons/cart.svg'),
          onPressed: () {},
        ),
        SizedBox(width: 10,)
      ],
    );
    
  }
}
